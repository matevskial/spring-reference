# Spring reference

This project contains some code examples that use various concepts of spring

This README document contains notes and tips for spring, 
other java and backend related technologies such as mave, hibernate, notes about general concept for backend development
such as REST APIs.

# Generating spring project with IntelliJ IDEA and Spring Initializr

* Dependencies:

    * Spring Web
    * Spring Data JPA
    * Spring Security
    * H2 Database
        
    * You can also add maven dependencies manually
    * If you get error ```Cannot find or load main class``` when running the application, close the project(```File -> Close project```) and restart the IDE.
    
# Notes

* Bean scopes define how and when are created the beans when they are injected as dependencies
    * Singleton(default) - only one instance is created and shared as dependency
    * Prototype - a new instance is created each time an other bean requests it
    * Request - a new instance is created per http request
    * Session - a new instance is created per http session
    
* Bean lifecycle
    * Bean initialized -> Dependencies Injected -> Spring internal processing ->  Custom init and destroy methods
    * Spring doesn't call destoy methods for prototype-scoped beans
        * You can create custom BeanProcessor class to handle this case
        
* Hibernate
    * ```SessionFactory``` that is created only once and is used to create ```Session```
    * After every ```session.beginTransaction() and session.getTransaction().commit()``` we should get new ```Session```
    object with method ```sessionFactory.getCurrentSession()```
    * Entity object lifecycle
        * object can be saved and managed by session once ```session.save(obj)``` is called
        * when commit/rollback/close on session, the object gets detached from the session
        * when merge, detached objects are again saved and managed
        * when delete/remove the object is removed
    * CascadeType.ALL is often useful for @OneToOne mapping and not for @ManyToOne or @ManyToMany
    * CascadeType.ALL is also useful for @OneToMany uni-directional mapping
    * Default Fetch types for mappings:
        * @OneToOne: FetchType.EAGER
        * @OneToMany: FetchType.LAZY
        * @ManyToOne: FetchType.EAGER
    * Create file ```import.sql``` in classpath(usually the resource folder)  to execute additional sql statements
    if auto-ddl is used(either create, create-drop or update options) instead ```data.sql```.
    * Use ```data.sql``` in conjuction with ```schema.sql```.

* HTTP Request methods
    * GET - request for resource(html file, javascript file, image)
        * used when a web site is visited
        * considered safe method
    
    * HEAD - like GET, but only for metadata, without the body
        * considered safe method
    
    * POST - used to post data to server
        * like a html form
        * It is a create request
    
    * PUT - used for the enclosed entity to be stored at the supplied URI
        * if the entity exists, it is supposed to be updated
        * It is a create or update request
    
    * DELETE - a request to delete a resource
    
    * TRACE - request to echo what was received at the server
        * Can be used to see if the request was altered by intermediate servers
        * considered safe method
    
    * OPTIONS - a way to request for what methods are supported
        * considered safe method

    * CONNECT - converts the request to a transparent TCP/IP tunnel
        * typically for HTTPS over unencrypted HTTP proxy
        
    * PATCH - Applies partial modifications to the specified resource
    
* HTTP Status codes
    * 100s are informational
    * 200s are successful
    * 300s are redirections
    * 400s are client errors
    * 500s are server side errors
 
* REST
    * It is an architectural style for building Web API
    * A RESTful service is a web service(or web API) that in its design includes these properties
        * Separation of Client and Server
        * Server requests are stateless
        * Cacheable requests
        * Uniform interface
        
* REST API Design best practices
    * Review API requirements
    * Identify main resources/entities
        * look at prominent "nouns"
            * for example: "artists"
            * use plural: "api/artists"
    * Assign HTTP method
        * POST - crate new entity
        * GET - read entities or entity
        * PUT - update existing entity(or batch of entities)
        * DELETE - delete existing entity
    
    * Example endpoints:
        * ```POST /api/artists```
        * ```GET /api/artists```
        * ```GET /api/artists/{artistId}```
        * ```PUT /api/artists``` 
        * ```PUT /api/artist/{artist_id}```
        * ```DELETE /api/artists/artistId```
        
    * Query parameters should be optional most of the time
        * usually used for non-resource properties, such as specifying sorting
    
    * It is good practice for API urls to be in form
        * api.yourdomain.com
        * yourdomain.com/api
    
    * When requesting to a resource wit a verb that is not implemented for that resource, 
    it is good practice to respond with status code 405 Method Not Allowed
    
    * When POST-ing with some data, it is good practice to respond with 400 Bad Request 
    if the data or part of the data that is being send is invalid
    
    * When POST-ing some data, it is good practice to respond with status code 201 Created
    and return as body the data  that was created, usually the returned data contains the assigned id
    
    * When PUT-ing some data, it is good practice to respond with status code 200 OK
    and return as body the data that was modified
    
    * When DELETE-ing some data, it is goot practice to respond with status code 200 OK
    without any body
    
    * Verbs that should be idempotent:
        * GET, PUT, PATCH, DELETE
        * Notice that POST is not idempotent
        
    * It is good practice to define some associations in APIs, like:
        * ```GET /api/artists/{artist_id}/songs```
        
    * Paging
        * using query params
        * wrap the paged list to give additional data about the page such as, url for next page, lenght of current page
        * ```GET /api/artists?page=1&pagesize=3```
        
    * Error handling
        * for some errors, it is good practice to respond with object describing the error
        
    * Caching
        * if supported by the server, caching is controlled by the header ```ETag```
        * a server will respond with 304 Not Modified if GET request is sent with the header ```If-None-Match```
        and the ETag is a tag of a recently cached resource
        * a server will respond with 412 Precondition Failed if PUT request is sent with the header ```If-MAtch```
        if the Etag is a tag of old cached resource
    
    * Versioning APIs
        * in URI path - ```http://service.com/api/v2/artists```
        * in query string - ```http://service.com/api/artists?v=2.0```
        * in HTTP headers 
            *```X-Version: 2.0```
            * ```Accept: application/json;version=2.0```
            * ```Content-Type: application/vnd.custom.content.type.v1+json```
    * Security
        * CORS(Cross Origin Resource Sharing)
            * browser makes 
        * Authentication methods
            * Basic auth
            * JWT
            * OAuth
            
    * Further readings
        * http://barelyenough.org/blog/2008/05/versioning-rest-web-services/
        * http://www.informit.com/articles/article.aspx?p=1566460
        * http://en.wikipedia.org/wiki/Representational_state_transfer
        * https://dzone.com/articles/rest-defining-a-resource
        * https://github.com/microsoft/api-guidelines/blob/vNext/Guidelines.md
   
* AOP and Spring AOP
    * Aspect Oriented Programming - a paradigm of programming for implementing logic for cross-cutting concern
    * use cases: logging, security, transactions, exception handlers
    * aspect: a module of code with logic that addresses some cross-cutting concern
    * advice: what action is taken and when it should be applied
        * Before advice - before method execution
        * After advice - after method execution
        * AfterReturning advice
        * AfterThrowing advice
        * Around advice
    * joint point: when to apply code during program execution
    * pointcut: A predicate expression for where advice should be applied
        * pattern syntax: ```execution(modifiers-pattern? return-type-pattern declaring-type-pattern?
                                        method-name-pattern(param-pattern) throws-pattern?)```
    * Spring AOP
        * every custom Aspect is must be annotated with @Aspect and @Component
    
* Maven
    * Official guides - http://maven.apache.org/guides/
    * Maven e-books - https://www.sonatype.com/ebooks
    * Maven cheatsheet - https://www.jrebel.com/blog/maven-cheat-sheet
    
    * Maven is a project and dependency manager
    * A project managed with maven has pom.xml(Project Object Model) on the root of the project
    
    * pom.xml file structure
        * project metadata
        * dependencies
        * plugins
            * contains confiuration for additional task to run
            
    * project coordinates
        * groupid - name of company, group, organization
            * specified with reverse domain name
        * artifactid - name of project
        * version - release version
        
    * archetypes
        * templates for specific project(like java project, web project)
        * maven-archetype-quickstart
        * maven-archetype-webapp
        
    * It is often necessary to add java version in pom.xml(because it defaults to java 1.5) 
        * either in ```properties```
        ```xml
      <properties>
          <maven.compiler.source>1.8</maven.compiler.source>
          <maven.compiler.target>1.8</maven.compiler.target>
      </properties>
      ```
      
        * or maven compiler plugin
        ```xml
       <plugin>
          <groupId>org.apache.maven.plugins</groupId>
          <artifactId>maven-compiler-plugin</artifactId>
          <version>${maven-compiler-plugin.version}</version>
              <configuration>
                <source>${java.version}</source>
                <target>${java.version}</target>
              </configuration>
      </plugin>
      ```
      
    * Maven private repository
        * for private(for example in a company) maven projects and modules
        * maven repository server managers:
            * Nexus, Artifactory, Archiva
            
    * Some maven plugins:
        * maven-compiler-plugin for compiler:compile
        * maven-surefire-plugin for surefire:test
        * maven-jar-plugin for jar:jar
    
    * Run spring application from the command line with Maven:
        * ```mvn spring-boot:run```