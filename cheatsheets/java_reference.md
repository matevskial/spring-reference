# Java reference

* [Misc](#misc)
* [Classes and interfaces](#classes-and-interfaces)
* [Data structures and collections](#data-structures-and-collections)
* [Java IO](#java-io)
* [Generic programming](#generic-programming)
* [Multithreading](#multithreading) 

## Misc

* ```java program.java``` compiles in-memory and executes, does not generat .class files

* ```var a = 100_000``` declaration of variable without specifying data type

* Object instance method ```finalize``` is deprecated since JDK 9

* Class hierarchies of exceptions:
```
    Throwable
    /      \
   /        \
Error   Exception(checked)
              |
         RuntimeExcetipn(unchecked)
```

* try/catch multicatch syntax
```java
try {
    // code
} catch(SQLException | IOException e) {
    // code
} catch (Exception e) {
    // code
}
```

* NullPointerException mitigation when comparing objects for equallity
```java
"abc".equals(otherStr);
Objects.equals(obj1, obj2);
```

* Constructors of wrapper classes(Integer, Long, etc) are deprecated since JDK 9

* try-with-resources example
    ```java
    try(FileReader in = new FileReader("filename)) {
        // code
    } catch (Exception e)
        // code
    }
    ```
  
* Defining the main method without static will result in runtime error

* Defining the main method without the required argument ```String[] args``` will result on runtime error

* var-args example
    ```java
    void fun(int ...numbers) {
        System.out.println(numbers.length);
    }
    ```
* var-arg must be last paramether of method

* ```Enum``` example
    ```java
    Enum ChargeLevel {
        FULL, HIGH, MEDIUM, LOW;
    }

    ChargeLevel h = ChargeLevel.HIGH;
    ChargeLEvel m = ChargeLevel.valueOf("MEDIUM");
    h == ChargeLevel.HIGH // results in true
    h.ordinal(); // results in 1
    ```
    * name of the enum should be single instead of plural
    * enum in java is implicitly a class

* Annotations example
    ```java
    @Override
    public void fun() {
        // code
    }
    ```
* Covariant return type of overriden method:
    * overriden method can return the same type or a subtype of the type that the overrided method returns

* The ```toString()``` method
   * the default implementation returns string containing class name and hashCode

* Runtime type checking
    * Suppose we have the following class hierarchy
    ```
	  Shape
        /
       /
     Circle
    ```

    ```java
    Circle circle = new Circle();

    circle instanceOf Circle // true
    circle instanceOf Shape // true
    circle instanceOf OtherClass // compile error
    null instanceOf Circle // false
    circle.getClass() == Circle.class // true
    Circle.class.isInstance(circle) // true
    ```
* The ```equals()``` method should:

    * be reflexive: ```x.equals(x) == true```
    * symetric: ```x.equals(y) == y.equals(x)
    * transivite: if ```x.equals(y) == true``` and ```y.equals(z) == true``, then ```x.equals(z) == true```
    * non-nullity: ```x.equals(null) == false```

* The ```equals()``` method checks for reference equallity by default

* Bitwise operators

    * ```<< // signed left shift, pads with zero```
    * ```>> // signed right shift, pads with current sign```
    * ```>>> // unsigned right shift, pads with zero```

* The ```Double.POSITIVE_INFINITY``` is larger than any ```Double``` or ```Integer```

* The ```Double.NaN``` is not equal to any ```Double``` or ```Integer```

* The expression ```Double.NaN == Double.NaN``` returns false

* If we have ```char[] arr = new char[]{'a', 'b', 'c'}```, then ```System.out.println(arr)``` will print the content of the array

* Importing static methods and fields from classes, or interfaces
    ```java
    import static java.lang.Math.*;
    System.out.println(sqrt(2));
    ```

* JSHELL is a Java REPL that can be used for experimenting with code snippets or libraries and APIs

* Regexp in Java

    * ```"abc".matches("abc"); // checks if the string matches the regex```
    * double escape for certain characters in regexp, one comes from java and one from the regexp
	* for the characters ```\, (, ), ?```
    * the wildcards ```.```, ```?``` match any character except new line
    * match one character from the set: ```[ab]```
    * match range of characters: ```[0-9]```, ```[a-z]```, ```[a-zA-Z]```
    * excepting characters: ```[^abc]```
    * alternation: ```(abc|efg)```
    * parentheses define groups
    * shorthands:
	* ```\\d``` is ```[0-9]```
	* ```\\D``` is ```[^0-9]```
	* ```\\s``` is ```[\\t\\n\\XOB\\f\\r]```
	* ```\\S``` is ```[^\\s]```
	* ```\\w``` is ```[a-zA-Z0-9]```
	* ```\\W``` is ```[^\\w]```
    * quantifiers
	* ```+``` one or more of preceeding group
	* ```*``` zero or more of preceeding group
	* ```{n}``` exactly n of preceeding group
	* ```{n,m}``` atleast n, but no more than m of preceeding group
	* ```{n,}``` atleast n of preceeding group

* If we import static members from classes like in the example
    ```java
    import static ClassName1.*;
    import static ClassName2.*;
    ```
    and there exist static members same identifiers in ```ClassName1``` and ```ClassName2```, 
    usage of it will result in compile-error.   

* BigInteger belongs to java.math package
    * use constants like ```BigInteger.ONE``` hwen possible, for some efficiency
    
* Streams
    
    ```java
    books.stream().filter(book -> { return book.getAuthor().startsWith("J");})
      .forEach(System.out::println);
   ```
  
    ```java
  // parallel stream, use only when having large dataset
  // and having more cores, in order to see performance improvements
  books.stream().filter(book -> { return book.getAuthor().startsWith("J");})
    .forEach(System.out::println);
    ```
  
* Module system
    * module is a group of related code(and maybe other resources)
    * must have an unique name(similar naming like packages)
    * by default everything in the module is hidden to the outside
    * module descriptor, can be used to specify what will be available to the outside world from the module 
    and what other code will be used by the module
        ```java
        module ModuleName {
          requires somemodule.name;
          exports somepackage.name;
        }
      ```

## Classes and interfaces

* Overriding method: OOP concept for implementing polymorphism

* Access control:
    * at top level(for classes, interfaces, enums): public or package private
    * member level: public, package-private, protected, private

* package-private vs protected:

    * protected is similar, only it allows members to be inherited by classes outside package

* Referencing subclass objects:
    * superclass reference
        * cannot access members specific to subclass
	* we can access overriden methods
	* can cast subclass to superclass and superclass to subclass
	* use when processing an array(or collection) and when method accepts superclass referemce
	* overriding is run-time polymorphism

* Interfaces can contain:
    * public constaints(are static)
    * default methods with implementation(keyword ```default```)
    * static method with implementation
    * private default methods since JDK 9

* Initiaization blocks inside class
    ```java
    class SomeClass {
    
        static {
            // declaring static members
        }
    
        static {
            // there may be more blocks
            // this may override some values
        }
    
        instance {
            // executed before any constructor
            // executed after superclass constructor
            // can access static members
        }
    }
    ```
* In abstract classes, static methods can't be abstract

* Anonymous classes example
    ```java
    new SuperClassOrInterface() {
        // fields can shadow other varialbes in the scope/context
        // methods can be defined
        // overriden methods can be defined
    
        // cannot have constructors
        // static variables should be final
        // cannot have static initializers
    }
    ```
* Anonymous classes should be used when only one instance is needed, or the body of the class is short, callbacks

## Data structures and collections

* Collections that are used as keys in Map must be immutable

* Instantiating immutable ```Set``` and ```Map```
    ```java
    Set<String> s = Set.of("abc", "123");

    Map<String, Integer> m = Map.of("abc", 1, "efg", 2);
    ```

    * ```LinkedHashSet``` and ```LinkedHashMap``` preseve the order of insertio, the data structures uses more memory

    * ```Queue<E>``` interface extends ```Collection<E>```
	* the method ```offer``` returns boolean
	* the method ```add``` throws ```IllegalStateException```
	* the method ```remove``` throws ```NoSuchElementException```
	* the methods ```poll``` and ```peek``` just return null
	* the methods ```offer```, ```peek```, ```poll``` are used more in practice

    * ```Deque<E>``` is prefered over ```Stack<E>```
	* can also use with capacity, making it size-restricted deque 

## Java IO

* ```Scanner``` can be also used to read a file, instead of standard input

## Generic programming

* generic classes
    ```java
    class GenericClass<T,V> {
        private T t;
        private U u;
    }
    
    // with specifying generic types on the right-side
    GenericClass<Integer, Character> n = new GenericClass<Integer, Character>();
    
    // without specifying generic types on the right-side
    GenericClass<Integer, Character> n = new GenericClass<>();
    
    // no generic types specified, the types are Object
    GenericClass n = new GenericClass();
    
    class GenericClass<T extends ClassOrInterface> {}
    
    class GenericClass<T extends A&B&C> {} // A can be class or interface, B, C, etc are always interfaces
    
    class GenericClass<? extends ClassOrInterface> {} // wildcard that specifies that any type that is subclass or implements the interface can be used
    
    class GenericClass<? super ClassOrInterface> {} // wildcard that specifies that any type that is superclass can be used
    ```

* generic methods
    ```java
    public static <T,V> void method(T t, V v) {}
    public <T> T getObj(T o) {}
    ```

* static methods can't use class's own type parameters

## Multithreading

* a thread is an independend unit of execution  that has its own stack and local variables
* a process can have one or multiple threads, and that what makes threads of one process connected, while multiple
processes of the same program are independent
* threads in a process share the memory space(global variables)
* in java, every program creates at least one thread
* the order of execution of threads is not defined
* example usecases
    * blocking IO(move reading of a file to a separate thread)
    * GUI applications
    * independent tasks
    
* using threads  with classes that extend the ```Thread``` class that should override the run method
    ```java
    
    class ThreadExample extends Thread {
        @Override
        public void run() {
          // some code
        }
    }
  
    public class Main {
        public static void main(String[] args){
          ThreadExample t1 = new ThreadExample(); //thread is in waiting state
          t1.start(); // thread is alive, after the code is finished running, the thread is in terminating state
        }
    } 
    ```

* using threads with classes that implement the ```Runnable``` interface, useful so the class can extend other class
    ```java
    
    class RunnableExample implements Runnable {
        @Override
        public void run() {
          // some code
        }
    }
  
    public class Main {
        public static void main(String[] args){
          Thread t1 = new Thread(new RunnableExample()); //thread is in waiting state
          t1.start(); // thread is alive, after the code is finished running, the thread is in terminating state
        }
    } 
    ```
  
* if it is expected that a method would be run by multiple threads, it is good practice to add the ```synchronized``` keyword to the signature of the method if the method uses some global variables
    * this means that only one thread can enter the method at a time
    ```java
  public synchronized void fun() {
      // do stuff
  }
    ```
    
* the keyword ```synchronized``` can be used to lock resources(such as object instances)
    ```java
    Object o = new Object;
    
    Thread t1 = new Thread(() -> {
        synchronized (o) {
            // do some stuff with o
        }
    })
    ```