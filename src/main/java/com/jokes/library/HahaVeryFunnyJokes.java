package com.jokes.library;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Component
@Profile("prod")
public class HahaVeryFunnyJokes implements Jokes {
    private List<String> jokes;

    public HahaVeryFunnyJokes() {
        jokes = new ArrayList<>();

        jokes.add("How does ear look like? It doesn't look, it hears");
        jokes.add("Do you have girlfriend? Yes, I have friends.");
    }

    @Override
    public String getRandomJoke() {
        return jokes.get(ThreadLocalRandom.current().nextInt(0, jokes.size()));
    }

    @Override
    public void addJoke(String joke) {
        jokes.add(joke);
    }
}
