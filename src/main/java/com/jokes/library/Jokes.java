package com.jokes.library;

/**
 * Interface for Jokes, used to demonstrade profile-specific spring components and beans
 */
public interface Jokes {
    String getRandomJoke();
    void addJoke(String joke);
}
