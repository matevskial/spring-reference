package com.jokes.library;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Component
@Profile({"default", "dev"})
public class OffensiveJokes implements Jokes {
    private List<String> jokes;

    public OffensiveJokes() {
        jokes = new ArrayList<>();

        jokes.add("Let me tell you a joke: Look in the mirror");
        jokes.add("You look like the jar nobody wants to put money in");
    }

    @Override
    public String getRandomJoke() {
        return jokes.get(ThreadLocalRandom.current().nextInt(0, jokes.size()));
    }

    @Override
    public void addJoke(String joke) {
        jokes.add(joke);
    }
}
