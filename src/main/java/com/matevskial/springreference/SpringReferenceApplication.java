package com.matevskial.springreference;

import com.jokes.library.Jokes;
import com.matevskial.springreference.beanlifecycledemo.ABean;
import com.matevskial.springreference.didemo.*;
import com.matevskial.springreference.persistence.model.GenericResource;
import com.matevskial.springreference.persistence.model.Song;
import com.matevskial.springreference.persistence.repository.GenericResourceRepository;
import com.matevskial.springreference.persistence.repository.SongRepository;
import com.matevskial.springreference.service.staticdata.StaticData;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.context.ApplicationContext;

import com.matevskial.springreference.service.mlog.LogService;
import org.springframework.context.annotation.ComponentScan;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;
import java.util.Optional;

/**
 * This is the application entry class.
 * When using @ComponentScan, you should also list the default package for scan
 */
@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
@ComponentScan(basePackages = {"com.jokes.library", "com.matevskial.springreference"})
@Slf4j
public class SpringReferenceApplication {

    public static void main(String[] args) {
        ApplicationContext ctx = SpringApplication.run(SpringReferenceApplication.class, args);

        workWithBeans(ctx);
        workWithJpa(ctx);
        workWithSlf4jLogger();
    }

    private static void workWithBeans(ApplicationContext ctx) {
        ctx.getBean("firstLogServiceImpl", LogService.class).log("A message");

        ctx.getBean("xmlInjectedBean", XmlInjectedBean.class).doStuff();

        InjectingValuesFromPropertiesFile inj1 =
                ctx.getBean(InjectingValuesFromPropertiesFile.class);
        InjectingValuesFromPropertiesFile inj2 =
                ctx.getBean(InjectingValuesFromPropertiesFile.class);
        System.out.println("Objects of type InjectingValuesFromPropertiesFile should not be singletons: " + !(inj1 == inj2));

        // use beans configured with annotations
        ctx.getBean("constructorInjectedBeanCustomId", ConstructorInjectedBean.class).doStuff();
        ctx.getBean(SetterInjectedBean.class).doStuff();
        ctx.getBean(FieldInjectedBean.class).doStuff();

        // use beans configured with java
        System.out.println(ctx.getBean("arrayListStaticData",StaticData.class).getEmails());
        System.out.println(ctx.getBean("linkedListStaticData",StaticData.class).getEmails());

        // use beans configured for specific profiles
        System.out.println("Profile-specific Jokes bean: " + ctx.getBean(Jokes.class).getRandomJoke());

        // get a bean that has lifecycle methods
        ctx.getBean(ABean.class);
    }

    private static void workWithJpa(ApplicationContext ctx) {
        testDbConnectionWithJdbc();
        workWithJpaRepository(ctx);
    }

    private static void workWithJpaRepository(ApplicationContext ctx) {
        GenericResourceRepository genericResourceRepository = ctx.getBean("genericResourceRepository", GenericResourceRepository.class);
        SongRepository songRepository = ctx.getBean(SongRepository.class);

        GenericResource resource = new GenericResource();
        resource.setName("A resource name");


        genericResourceRepository.save(resource);
        System.out.println("saved generic resource");

        Optional<List<Song>> optionalSongs =  songRepository.findByAlbumName("Force Majeure");
        if(optionalSongs.isEmpty()) {
            System.out.println("No songs found for that album name");
        } else {
            System.out.println("Number of songs for that album found: " + optionalSongs.get().size());
        }
    }

    private static void testDbConnectionWithJdbc() {
        String jdbcUrl = "jdbc:h2:file:./data/mydb;DB_CLOSE_ON_EXIT=FALSE;AUTO_RECONNECT=TRUE";
        String username = "admin";
        String password = "admin";
        try {
            Connection myConn =
                    DriverManager.getConnection(jdbcUrl, username, password);
            System.out.println("Connection to h2 using jdbc successful");
            myConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void workWithSlf4jLogger() {
        log.info("Info level log");
        log.debug("Debug level log");
        log.error("Error level log");
    }
}
