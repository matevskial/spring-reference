package com.matevskial.springreference.aopdemo;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ServiceLoggerAspect {

    @Before("execution(* com.matevskial.springreference.service.staticdata.ArrayListStaticData.*(..))")
    public void beforeStaticDataMethodsAdvice() {
        System.out.println("Aspect logger: A method from the StaticData service was called.");
    }
}
