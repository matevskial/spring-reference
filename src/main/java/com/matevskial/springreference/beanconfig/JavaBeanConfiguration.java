package com.matevskial.springreference.beanconfig;

import com.matevskial.springreference.service.mlog.LogService;
import com.matevskial.springreference.service.staticdata.StaticData;
import com.matevskial.springreference.service.staticdata.StaticDataFactory;
import org.springframework.core.env.Environment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.*;

/**
 * This class exists to configure beans using Java(no XML)
 *  * note: the name of the method is the id of the bean
 *  * note: when spring initalizes, methods that create singleton-scoped beans
 *    will be called regardless of explicit need for this bean
 *  * note: when spring initalizes, method that create prototype-scoped beans
 *    will be not called until explicit need for this bean
 */
@Configuration
@PropertySources({
        @PropertySource("classpath:java-imported-properties.properties")
})
public class JavaBeanConfiguration {
    private LogService logService;

    @Autowired
    Environment env;

    @Value("${foo.value1}")
    private String value1;

    @Value("${foo.value2}")
    private String value2;

    public JavaBeanConfiguration(@Qualifier("firstLogServiceImpl") LogService logService) {
        this.logService = logService;
    }

    @Bean
    @Primary
    StaticData arrayListStaticData() {

        logService.log("Creating the bean arrayListStaticData ");
        logService.log("Values from properties: " + value1 + " " + value2);
        logService.log("Values from env ENV_EXAMPLE=" + env.getProperty("ENV_EXAMPLE"));
        return new StaticDataFactory().createStaticData("arraylist");

    }

    // note: when spring initalizes, this method will be not called
    // until explicit need for this bean
    @Bean
    @Scope("prototype")
    StaticData linkedListStaticData() {
        logService.log("Creating the bean linkedListStaticData");
        return new StaticDataFactory().createStaticData("arraylist");

    }
}
