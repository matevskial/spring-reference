package com.matevskial.springreference.beanconfig;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * This class exists to tell spring boot to load bean configuration from an xml file
 */
@Configuration
@ImportResource({"classpath*:applicationContext.xml"})
public class XmlBeanConfiguration {
}
