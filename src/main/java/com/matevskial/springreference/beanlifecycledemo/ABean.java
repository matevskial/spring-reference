package com.matevskial.springreference.beanlifecycledemo;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class ABean implements InitializingBean, DisposableBean, BeanNameAware,
        BeanFactoryAware, ApplicationContextAware {

    public ABean() {
        System.out.println("I'm in the ABean, a bean for lifecycle demo");
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        System.out.println("The lifecycle bean has its properties set");
    }

    @Override
    public void destroy() throws Exception {
        System.out.println("The lifecycle bean has been terminated");
    }

    @Override
    public void setBeanName(String name) {
        System.out.println("The lifecycle bean has called setBeanName");
    }

    @Override
    public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
        System.out.println("The lifecycle bean has called setBeanFactory");
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        System.out.println("The lifecycle bean has called setApplicationContext");
    }

    public void beforeInit() {
        System.out.println("Before Init - called by Bean Post Processor");
    }

    public void afterInit() {
        System.out.println("After Init - called by Bean Post Processor");
    }

    @PostConstruct
    // this is akin to init-method for beans configured in xml file
    public void postConstruct() {
        System.out.println("The @PostConstruct annotated method has been called");
    }

    @PreDestroy
    // this is akin to destroy-method for beans configured in xml file
    public void preDestroy() {
        System.out.println("The @PreDestroy annotated method has been called");
    }
}
