package com.matevskial.springreference.bootstrap;

import com.jokes.library.Jokes;
import com.matevskial.springreference.persistence.model.Album;
import com.matevskial.springreference.persistence.model.Artist;
import com.matevskial.springreference.persistence.model.FileEntry;
import com.matevskial.springreference.persistence.model.Song;
import com.matevskial.springreference.persistence.repository.AlbumRepository;
import com.matevskial.springreference.persistence.repository.ArtistRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

/**
 * A class that listens to an Application event
 * This class listens to an event where the context is starting or context is refresing
 * Such class can be used to write code that will execute when spring application starts,
 * for example to load initial data.
 */
@Slf4j
@Component
@Profile({"default", "dev"})
public class DevEnvAppBootstrap implements ApplicationListener<ContextRefreshedEvent> {

    private Jokes jokes;

    private ArtistRepository artistRepository;
    private AlbumRepository albumRepository;

    public DevEnvAppBootstrap(Jokes jokes, ArtistRepository artistRepository, AlbumRepository albumRepository) {
        this.jokes = jokes;

        this.artistRepository = artistRepository;
        this.albumRepository = albumRepository;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        log.info("Start bootstrapping");
        doStuff();
        initData();
        log.info("End bootstrapping");
    }

    private void doStuff() {
        System.out.println(jokes.getRandomJoke());
    }

    private void initData() {
        Artist td = new Artist("Tangerine Dream");

        FileEntry f =new FileEntry("cf.mp3");
        Song cf = new Song(601, "Cloudburst Flight", f);
        f.setSong(cf);
        cf.getArtists().add(td);

        Album fm = new Album("Force Majeure");
        fm.getArtists().add(td);
        fm.getSongs().add(cf);
        fm.setImage(new Byte[]{(byte) 2, (byte) 3});
        cf.setAlbum(fm);

        artistRepository.save(td);
        albumRepository.save(fm);
    }
}
