package com.matevskial.springreference.didemo;

import com.matevskial.springreference.service.mlog.LogService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * An example bean class to demonstrate IOC and DI through constructor and configured using annotations
 * This is the recommended way of DI
 *  * because the dependencies are hidden in the class,
 *    and the dependency injector can only inject through the constructor
 *  * because there is like a 'contract' where
 *    the class has to be injected with its dependencies in order to be used
 *  * because no need to autowire manually the dependencies
 *    but you could still annotate the constructor with @Autowired
 */
@Component("constructorInjectedBeanCustomId")
public class ConstructorInjectedBean {
    private LogService logService;

    public ConstructorInjectedBean(@Qualifier("secondLogServiceImpl") LogService logService) {
        this.logService = logService;
    }

    public String doStuff() {
        logService.log("function doStuff from ConstructorInjectedBean");
        return "done";
    }
}
