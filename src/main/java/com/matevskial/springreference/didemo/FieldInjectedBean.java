package com.matevskial.springreference.didemo;

import com.matevskial.springreference.service.mlog.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * An example bean class to demonstrate IOC and DI through fields and configured using annotations
 * This is the most not recommended way of DI
 *  * because users of the class are allowed to change the injected dependency
 */
@Component
public class FieldInjectedBean {
    @Autowired
    @Qualifier("firstLogServiceImpl")
    public LogService logService;

    public String doStuff() {
        logService.log("function doStuff from FieldInjectedBean");
        return "done";
    }
}
