package com.matevskial.springreference.didemo;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
public class InjectingValuesFromPropertiesFile {
    private String value1;
    private String value2;

    public void printValues() {
        System.out.println(String.format("value1=%s; value2=%s", value1, value2));
    }
}
