package com.matevskial.springreference.didemo;

import com.matevskial.springreference.service.mlog.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 * An example bean class to demonstrate IOC and DI through setter and configured using annotations
 * This is also not recommended way of DI
 *  * because users of the class are allowed to change the injected dependency
 *    with the setter
 */
@Component
public class SetterInjectedBean {
    private LogService logService;

    public String doStuff() {
        logService.log("function doStuff from SetterInjectedBean");
        return "done";
    }

    @Autowired
    @Qualifier("secondLogServiceImpl")
    public void setLogService(LogService logService) {
        this.logService = logService;
    }
}
