package com.matevskial.springreference.didemo;

import com.matevskial.springreference.service.mlog.LogService;

/**
 * An example bean class to demonstrate DI that is configured with XML
 * Notice that this class is not annotated as spring component(@Controller, etc...)
 * so configuration for DI is done in the applicationContext.xml file
 */
public class XmlInjectedBean {
    private LogService logService;
    // class field to demonstrate setter DI
    private LogService logService1;

    public XmlInjectedBean(LogService logService) {
        this.logService = logService;
    }

    public String doStuff() {
        logService.log("function doStuff from XmlInjectedController");
        logService1.log("function doStuff from XmlInjectedController logged with logService1");
        return "done";
    }

    // setter to demonstrate the DI with setter
    public void setLogService1(LogService logService1) {
        this.logService1 = logService1;
    }

    // init bean lifecycle method configured with xml
    // can be of any access modifier, typically void and no arg
    public void doStartupStuff() {
        System.out.println("doStartupStuff from XmlInjectedCOntroller");
    }

    // init bean lifecycle method configured with xml
    // can be of any access modifier, typically void and no arg
    public void doCleanupStuff() {
        System.out.println("doCleanStuff from XmlInjectedCOntroller");
    }
}
