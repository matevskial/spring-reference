package com.matevskial.springreference.hibernatedemo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * An Entity class to demonstrate @ManyToMany mapping
 */
@Entity
@Table(name="actor")
@Getter
@Setter
@ToString(exclude = {"movies"})
public class Actor {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @ManyToMany
    @JoinTable(
            name="movie_actor",
            joinColumns = @JoinColumn(name="actor_id"),
            inverseJoinColumns = @JoinColumn(name="movie_id")
    )
    private List<Movie> movies;

    public Actor() {

    }


    public Actor(String name) {
        this.name = name;
    }

    public void addMovie(Movie m) {
        if(movies == null) {
            movies = new ArrayList<>();
        }

        movies.add(m);
    }
}
