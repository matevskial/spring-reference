package com.matevskial.springreference.hibernatedemo;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.springframework.context.ApplicationContext;

import java.util.List;

public class HibernateDemoDriverCode {

    private static SessionFactory getSessionFactory() {
        SessionFactory factory = new Configuration()
                .configure("classpath:hibernate.cfg.xml")
                .addAnnotatedClass(Movie.class)
                .addAnnotatedClass(MovieDetail.class)
                .addAnnotatedClass(MovieLeadingActor.class)
                .addAnnotatedClass(MovieLanguage.class)
                .addAnnotatedClass(MovieReview.class)
                .addAnnotatedClass(Actor.class)
                .buildSessionFactory();
        return factory;
    }

    public static void workWithHibernate() {
        SessionFactory factory = getSessionFactory();


        Session hibernateSession = factory.getCurrentSession();
        try {
            // insert data
            hibernateSession.beginTransaction();
            saveMovieWithHibernate(hibernateSession, "Good Will Hunting");
            saveMovieWithHibernate(hibernateSession, "A few good men");
            saveMovieWithHibernate(hibernateSession, "Primal Fear");
            hibernateSession.getTransaction().commit();

            // query a movie by id
            hibernateSession = factory.getCurrentSession();
            hibernateSession.beginTransaction();
            Movie getMovie = hibernateSession.get(Movie.class, 1L);
            System.out.println("Queried the movie: " + getMovie);
            hibernateSession.getTransaction().commit();

            // query a list of movies
            hibernateSession = factory.getCurrentSession();
            hibernateSession.beginTransaction();
            List<Movie> movies = hibernateSession
                    .createQuery("from Movie m where m.id > 1L", Movie.class).getResultList();
            System.out.println("The movies: " + movies);
            hibernateSession.getTransaction().commit();

            // update a movie
            hibernateSession = factory.getCurrentSession();
            hibernateSession.beginTransaction();
            Movie tobeUpdatedMovie = hibernateSession.get(Movie.class, 1L);
            tobeUpdatedMovie.setName("Heat");
            hibernateSession.getTransaction().commit();
            System.out.println("Done updating a movie");

            // update a movie with createQuery
            hibernateSession = factory.getCurrentSession();
            hibernateSession.beginTransaction();
            hibernateSession.createQuery("update Movie set name='Good Will Hunting' where name='Heat'")
                    .executeUpdate();
            System.out.println("Done updating a movie with createQuery");
            hibernateSession.getTransaction().commit();

            // delete a movie
            hibernateSession = factory.getCurrentSession();
            hibernateSession.beginTransaction();
            Movie tobeDeletedMovie = hibernateSession.get(Movie.class, 2L);
            hibernateSession.delete(tobeDeletedMovie);
            hibernateSession.getTransaction().commit();
            System.out.println("Deleted movie with id=2");

            // delete a movie with createQuery
            hibernateSession = factory.getCurrentSession();
            hibernateSession.beginTransaction();
            hibernateSession.createQuery("delete from Movie where id=3L")
                    .executeUpdate();
            hibernateSession.getTransaction().commit();
            System.out.println("Deleted movie with id=3");

        } finally {
            hibernateSession.close();
        }
    }

    private static void saveMovieWithHibernate(Session session, String movieName) {
        Movie m = new Movie(movieName);

        session.save(m);

        System.out.println("Movie is saved");
    }

    private static void workWithHibernateAdvancedMappings() {
        SessionFactory factory = getSessionFactory();

        workWithHibernateOneToOneUnidirectional(factory);
        workWithHibernateOneToOneBidirectional(factory);
        workWithHibernateManyToOneBidirectional(factory);
        workWithHibernateManyToOneUnidirectional(factory);
        workWithHibernateManyToMany(factory);

        factory.close();
    }

    private static void workWithHibernateOneToOneUnidirectional(SessionFactory factory) {
        Session hibernateSession = null;

        // insert new movie and move_detail that have @OneToOne mapping
        Movie m1 = new Movie("The Devils Advocate");
        MovieDetail m1Detail = new MovieDetail("220");
        m1Detail.setMovie(m1);
        hibernateSession = factory.getCurrentSession();
        hibernateSession.beginTransaction();
        hibernateSession.save(m1Detail);
        hibernateSession.getTransaction().commit();
        System.out.println("Saved movie_detail along with movie for @OneToOne mapping");

        // delete the newly added movie_detail and the movie along with it
        hibernateSession = factory.getCurrentSession();
        hibernateSession.beginTransaction();
        m1Detail = hibernateSession.get(MovieDetail.class, 1L);
        hibernateSession.delete(m1Detail);
        System.out.println("");
        hibernateSession.getTransaction().commit();
        System.out.println("Deleted movie_deteail along with movie for @OneToOne mapping");
    }

    private static void workWithHibernateOneToOneBidirectional(SessionFactory factory) {
        Session hibernateSession = null;

        // insert new movie and movie_leading_actor
        Movie m2 = new Movie("The Departed");
        MovieLeadingActor m2LeadingActor = new MovieLeadingActor("Matt Damon");
        m2.setMovieLeadingActor(m2LeadingActor);
        m2LeadingActor.setMovie(m2);
        hibernateSession = factory.getCurrentSession();
        hibernateSession.beginTransaction();
        hibernateSession.save(m2);
        System.out.println("Saved movie along with movie_leading_actor that are @OneToOne bi-directionally mapped");
        hibernateSession.getTransaction().commit();
    }

    private static void workWithHibernateManyToOneBidirectional(SessionFactory factory) {
        // test @ManyToOne and @OneToMany: create two movie languages and two movies, delete one of each
        Session hibernateSession = null;

        hibernateSession = factory.getCurrentSession();
        hibernateSession.beginTransaction();
        Movie m1 = new Movie("Reservoir dogs");
        Movie m2 = new Movie("Timecrimes");
        MovieLanguage ml1 = new MovieLanguage("English");
        MovieLanguage ml2 = new MovieLanguage("Spanish");
        ml1.addMovie(m1);
        ml2.addMovie(m2);

        hibernateSession.save(m1);
        hibernateSession.save(m2);
        hibernateSession.save(ml1);
        hibernateSession.save(ml2);
        hibernateSession.getTransaction().commit();
        System.out.println("Saved movie along with movie_language for @OneToMany mapping");

        hibernateSession = factory.getCurrentSession();
        hibernateSession.beginTransaction();
        Movie m3 = hibernateSession.createQuery("from Movie where name='Reservoir dogs'", Movie.class).getSingleResult();
        hibernateSession.delete(m3);
        hibernateSession.getTransaction().commit();
        System.out.println("Deleted one movie, should not delete the language it was associated with");

        hibernateSession = factory.getCurrentSession();
        hibernateSession.beginTransaction();

        Query<MovieLanguage> query =
                hibernateSession.createQuery("select ml from MovieLanguage ml "
                        + "join fetch ml.movies "
                        + "where ml.id=:movie_language_id", MovieLanguage.class);

        query.setParameter("movie_language_id", 2L);
        MovieLanguage ml = query.getSingleResult();
        System.out.println("The movie language queried with HQL: " + ml.getLanguage());

        hibernateSession.getTransaction().commit();
        System.out.println("Queried move language languages with HQL");
    }

    private static void workWithHibernateManyToOneUnidirectional(SessionFactory factory) {
        Session hibernateSession;

        // insert new movie with some reviews
        Movie m1 = new Movie("Cats");

        m1.addReview(new MovieReview("Bad animation"));
        m1.addReview(new MovieReview("Not for kids!"));

        hibernateSession = factory.getCurrentSession();
        hibernateSession.beginTransaction();
        hibernateSession.save(m1);
        hibernateSession.getTransaction().commit();
        System.out.println("Saved a bad movie along its review, for @OneToMany unidirectional mapping");

        hibernateSession = factory.getCurrentSession();
        hibernateSession.beginTransaction();
        Movie badMovie = hibernateSession
                .createQuery("from Movie where name='Cats'", Movie.class).getSingleResult();
        hibernateSession.delete(badMovie);
        hibernateSession.getTransaction().commit();
        System.out.println("The bad movie is deleted, reviews should be deleted along with it");
    }

    private static void workWithHibernateManyToMany(SessionFactory factory) {
        Session hibernateSession;

        // insert some dummy movies and dummy actors
        Movie m1 = new Movie("Movie A");
        Movie m2 = new Movie("Movie B");
        Actor a1 = new Actor("Actor A");
        Actor a2 = new Actor("Actor B");
        Actor a3 = new Actor("Actor C");

        m1.addActor(a1);
        m1.addActor(a2);
        m2.addActor(a3);

        hibernateSession = factory.getCurrentSession();
        hibernateSession.beginTransaction();
        hibernateSession.save(m1);
        hibernateSession.save(m2);
        hibernateSession.save(a1);
        hibernateSession.save(a2);
        hibernateSession.save(a3);
        hibernateSession.getTransaction().commit();
        System.out.println("Saved some movies and actor and their associations, for @ManyToMany mapping");

        hibernateSession = factory.getCurrentSession();
        hibernateSession.beginTransaction();
        Movie m3 =
                hibernateSession.createQuery("from Movie where name = 'Movie A'", Movie.class).getSingleResult();
        hibernateSession.delete(m3);
        hibernateSession.getTransaction().commit();
        System.out.println("Deleted one movie, should delete associations put in table movie_actor, for @ManyToMany mapping");
    }
}
