package com.matevskial.springreference.hibernatedemo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * An Entity class to demonstrate working with Hibernate
 */
@Entity
@Table(name="movie")
@Getter
@Setter
@ToString(exclude = {"movieLanguage", "movieReviews", "actors"})
public class Movie {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "name")
    private String name;

    @OneToOne(mappedBy = "movie", cascade = { CascadeType.DETACH, CascadeType.MERGE,
        CascadeType.PERSIST, CascadeType.REFRESH
    })
    private MovieLeadingActor movieLeadingActor;

    @ManyToOne(cascade = { CascadeType.DETACH, CascadeType.MERGE,
            CascadeType.PERSIST, CascadeType.REFRESH},
            fetch = FetchType.LAZY
    )
    @JoinColumn(name="movie_language_id")
    private MovieLanguage movieLanguage;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "movie_id")
    private List<MovieReview> movieReviews;

    @ManyToMany
    @JoinTable(
            name="movie_actor",
            joinColumns = @JoinColumn(name="movie_id"),
            inverseJoinColumns = @JoinColumn(name="actor_id")
    )
    private List<Actor> actors;

    public Movie() {

    }

    public Movie(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void addReview(MovieReview review) {
        if(movieReviews == null) {
            movieReviews = new ArrayList<>();
        }

        movieReviews.add(review);
    }

    public void addActor(Actor actor) {
        if(actors == null) {
            actors = new ArrayList<>();
        }

        actors.add(actor);
    }
}
