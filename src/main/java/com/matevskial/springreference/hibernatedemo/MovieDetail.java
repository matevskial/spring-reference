package com.matevskial.springreference.hibernatedemo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

/**
 * An Entity class to demonstrate @OneToOne uni-directional mapping
 */
@Entity
@Table(name="movie_detail")
@Getter
@Setter
@ToString
public class MovieDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name="length_minutes")
    String lengthMinutes;

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="movie_id")
    Movie movie;

    public MovieDetail() {

    }

    public MovieDetail(String lengthMinutes) {
        this.lengthMinutes = lengthMinutes;
    }
}
