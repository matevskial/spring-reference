package com.matevskial.springreference.hibernatedemo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * An Entity class to demonstrate @ManyToOne and @OneToMany bi-directional mappings
 */
@Entity
@Table(name = "movie_language")
@Getter
@Setter
@ToString
public class MovieLanguage {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "language")
    private String language;

    @OneToMany(mappedBy = "movieLanguage",
            cascade = { CascadeType.DETACH, CascadeType.MERGE,
                    CascadeType.PERSIST, CascadeType.REFRESH})
    private List<Movie> movies;

    public MovieLanguage() {
    }

    public MovieLanguage(String language) {
        this.language = language;
    }

    /**
     * Convenient method to set the bi-directional mapping
     * @param m
     */
    public void addMovie(Movie m) {
        if(movies == null) {
            movies = new ArrayList<>();
        }
        movies.add(m);
        m.setMovieLanguage(this);
    }
}
