package com.matevskial.springreference.hibernatedemo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

/**
 * An Entity class to demonstrate @OneToOne bi-directional mapping
 */
@Entity
@Table(name="movie_leading_actor")
@Getter
@Setter
@ToString
public class MovieLeadingActor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name="actor_name")
    private String actorName;

    @OneToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="movie_id")
    private Movie movie;

    public MovieLeadingActor() {

    }

    public MovieLeadingActor(String actorName) {
        this.actorName = actorName;
    }
}
