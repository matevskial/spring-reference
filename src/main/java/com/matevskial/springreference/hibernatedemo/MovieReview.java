package com.matevskial.springreference.hibernatedemo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;

/**
 * An Entity class to demonstrate @OneToMany uni-directional mapping
 */
@Entity
@Table(name="movie_reviews")
@Getter
@Setter
@ToString
public class MovieReview {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "review")
    private String review;

    public MovieReview() {
    }

    public MovieReview(String review) {
        this.review = review;
    }
}
