package com.matevskial.springreference.persistence.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A POJO class that represents a model for an Album
 * Used by JPA for ORM
 */
@Data
@ToString(exclude = {"songs"})
@EqualsAndHashCode(of = "id")
@Entity
public class Album {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "album")
    private List<Song> songs = new ArrayList<>();

    @ManyToMany
    private Set<Artist> artists = new HashSet<>();

    @Lob // Binary large object
    private Byte[] image;

    public Album() {

    }

    public Album(String name) {
        this.name = name;
    }

    public Album(String name, List<Song> songs, Set<Artist> artists) {
        this.name = name;
        this.songs = songs;
        this.artists = artists;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Song> getSongs() {
        return songs;
    }

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }

    public Set<Artist> getArtists() {
        return artists;
    }

    public void setArtists(Set<Artist> artists) {
        this.artists = artists;
    }

    public Byte[] getImage() {
        return image;
    }

    public void setImage(Byte[] image) {
        this.image = image;
    }
}
