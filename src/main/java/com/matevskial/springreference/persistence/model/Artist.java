package com.matevskial.springreference.persistence.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * A POJO class that represents a model for an Artist
 * Used by JPA for ORM
 */
@Entity
public class Artist {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    @ManyToMany(mappedBy = "artists")
    private Set<Song> songs = new HashSet<>();

    public Artist() {

    }

    public Artist(String name) {
        this.name = name;
    }

    public Artist(String name, Set<Song> songs) {
        this.name = name;
        this.songs = songs;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Song> getSongs() {
        return songs;
    }

    public void setSongs(Set<Song> songs) {
        this.songs = songs;
    }
}
