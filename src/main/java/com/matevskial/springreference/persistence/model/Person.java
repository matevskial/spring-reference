package com.matevskial.springreference.persistence.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Person {
    private long id;
    private String name;
    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private String email;

    public Person(long id, String name, String email) {
        this.id= id;
        this.name = name;
        this.email = email;
    }

    public Person(long id, String name) {
        this.id= id;
        this.name = name;
        this.email = "";
    }
}
