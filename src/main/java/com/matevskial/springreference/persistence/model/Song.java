package com.matevskial.springreference.persistence.model;

import lombok.Data;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A POJO class that represents a model for a Song
 * Used by JPA for ORM
 */
@Data
@Entity
public class Song {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer length;

    private String name;

    @ManyToMany
    @JoinTable(name = "song_artist", joinColumns = @JoinColumn(name = "song_id", referencedColumnName = "id"),
    inverseJoinColumns = @JoinColumn(name = "artist_id", referencedColumnName = "id"))
    private Set<Artist> artists = new HashSet<>();

    @OneToOne(cascade = CascadeType.ALL) // if the song gets deleted, the file entry also gets deleted
    private FileEntry file;

    @ManyToOne(fetch = FetchType.LAZY)
    private Album album;

    public Song() {
    }

    public Song(Integer length, String name, FileEntry file) {
        this.length = length;
        this.name = name;
        this.file = file;
    }

    public Song(Integer length, String name, FileEntry file, Set<Artist> artists) {
        this.length = length;
        this.name = name;
        this.file = file;
        this.artists = artists;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Song song = (Song) o;
        return id.equals(song.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "Song{" +
                "id=" + id +
                ", length=" + length +
                ", name='" + name + '\'' +
                '}';
    }
}
