package com.matevskial.springreference.persistence.repository;

import com.matevskial.springreference.persistence.model.Album;
import org.springframework.data.repository.CrudRepository;

public interface AlbumRepository extends CrudRepository<Album, Long> {
}
