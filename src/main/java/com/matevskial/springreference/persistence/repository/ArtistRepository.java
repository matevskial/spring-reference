package com.matevskial.springreference.persistence.repository;

import com.matevskial.springreference.persistence.model.Artist;
import org.springframework.data.repository.CrudRepository;

public interface ArtistRepository extends CrudRepository<Artist, Long> {
}
