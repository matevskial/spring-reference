package com.matevskial.springreference.persistence.repository;

import com.matevskial.springreference.persistence.model.FileEntry;
import org.springframework.data.repository.CrudRepository;

public interface FileEntryRepository extends CrudRepository<FileEntry, Long> {
}
