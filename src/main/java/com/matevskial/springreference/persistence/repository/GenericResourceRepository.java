package com.matevskial.springreference.persistence.repository;

import com.matevskial.springreference.persistence.model.GenericResource;
import org.springframework.data.repository.CrudRepository;

public interface GenericResourceRepository extends CrudRepository<GenericResource, Long> {
}
