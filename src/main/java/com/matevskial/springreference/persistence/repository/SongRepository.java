package com.matevskial.springreference.persistence.repository;

import com.matevskial.springreference.persistence.model.Song;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

/**
 * Repository for the Song entity
 * One way to specify a query is using the method name.
 *  * for example, if we want to find songs that are in Album with given name, the method name would be findByAlbumName
 *  and would receive name as parameter
 */
public interface SongRepository extends CrudRepository<Song, Long> {
    Optional<Song> findByName(String name);
    Optional<List<Song>> findByAlbumName(String albumName);
}
