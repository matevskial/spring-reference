package com.matevskial.springreference.rest;

import com.matevskial.springreference.persistence.model.GenericResource;
import com.matevskial.springreference.persistence.repository.GenericResourceRepository;
import com.matevskial.springreference.rest.exceptions.GenericResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/genericresource")
public class GenericResourceRestController {

    @Autowired
    private GenericResourceRepository genericResourceRepository;

    @GetMapping("all")
    public ResponseEntity<Iterable<GenericResource>> getAllGenericResources() {
        return ResponseEntity.ok(genericResourceRepository.findAll());
    }

    @GetMapping("{id}")
    public ResponseEntity<GenericResource> getGenericResourceById(@PathVariable Long id) {
        Optional<GenericResource> genericResource = genericResourceRepository.findById(id);

        if(genericResource.isEmpty()) {
            throw new GenericResourceNotFoundException("GenericResource id not found - " + id);
        }

        return ResponseEntity.ok(genericResource.get());
    }
}
