package com.matevskial.springreference.rest;

import com.matevskial.springreference.rest.exceptions.GenericResourceNotFoundException;
import com.matevskial.springreference.rest.responses.GenericResourceErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice()
public class GenericResourceRestExceptionHandler {

    @ExceptionHandler
    public ResponseEntity<GenericResourceErrorResponse> handleException(GenericResourceNotFoundException e) {
        GenericResourceErrorResponse r = new GenericResourceErrorResponse();
        r.setStatus(HttpStatus.NOT_FOUND.value());
        r.setMessage(e.getMessage());
        r.setTimestamp(System.currentTimeMillis());

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(r);
    }
}
