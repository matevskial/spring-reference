package com.matevskial.springreference.rest;


import com.matevskial.springreference.persistence.model.Person;
import com.matevskial.springreference.service.staticdata.StaticDataService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.atomic.AtomicLong;

@RestController
public class PersonRestController {
    private final AtomicLong counter = new AtomicLong();

    private final StaticDataService staticDataService;

    public PersonRestController(StaticDataService staticDataService) {
        this.staticDataService = staticDataService;
    }

    /**
     * Returns a json representation of model Person
     * Optionally includes  email JSON field with random value
     * The random email is provided using the service staticDataService
     * @param includeEmail Used to decide whether  to include email JSON field or not
     * @return object of the model Person
     */
    @GetMapping("/person")
    public Person user(@RequestParam(value = "email", defaultValue = "false") Boolean includeEmail) {
        if(includeEmail) {
            String email = staticDataService.getRandomEmail();

            return new Person(counter.incrementAndGet(), "some_username", email);
        } else {
            return new Person(counter.incrementAndGet(), "sampleName");
        }
    }
}
