package com.matevskial.springreference.rest.exceptions;

public class GenericResourceNotFoundException extends RuntimeException {
    public GenericResourceNotFoundException(String message) {
        super(message);
    }
}
