package com.matevskial.springreference.rest.responses;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.Builder;
import lombok.NoArgsConstructor;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class GenericResourceErrorResponse {
    private int status;
    private String message;
    private long timestamp;
}
