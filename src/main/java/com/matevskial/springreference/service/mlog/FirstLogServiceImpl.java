package com.matevskial.springreference.service.mlog;

import org.springframework.context.annotation.Primary;

/**
 * This class is an XML configured bean
 * The annotation @Primary specifies that this Bean will be selected to be injected as a dependency for the interface LogService
 */
@Primary
public class FirstLogServiceImpl implements LogService {

    @Override
    public void log(String message) {
        System.out.println("FirstLogServiceImpl: " + message);
    }
}
