package com.matevskial.springreference.service.mlog;

/**
 * An example interface for a log service, that accompanies the code for IOC and DI demos
 */
public interface LogService {
    void log(String message);
}
