package com.matevskial.springreference.service.mlog;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class SecondLogServiceImpl implements LogService {

    @Override
    public void log(String message) {
        System.out.println("SecondLogServiceImpl: " + message);
    }
}
