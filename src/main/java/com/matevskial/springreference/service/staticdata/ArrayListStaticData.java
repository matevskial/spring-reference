package com.matevskial.springreference.service.staticdata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ArrayListStaticData implements StaticData {
    @Override
    public List<String> getEmails() {
        return new ArrayList<>(Arrays.asList(
                "em1@some",
                "em2@some",
                "em3@some",
                "em4@some"
        ));
    }
}