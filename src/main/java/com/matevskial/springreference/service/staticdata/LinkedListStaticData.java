package com.matevskial.springreference.service.staticdata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class LinkedListStaticData implements StaticData {
    @Override
    public List<String> getEmails() {
        return new LinkedList<>(Arrays.asList(
                "em1@some",
                "em2@some",
                "em3@some",
                "em4@some"
        ));
    }
}
