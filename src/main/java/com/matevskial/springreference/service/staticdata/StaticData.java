package com.matevskial.springreference.service.staticdata;

import java.util.List;

/**
 * A service to provide colection of some static data
 */
public interface StaticData {
    List<String> getEmails();
}
