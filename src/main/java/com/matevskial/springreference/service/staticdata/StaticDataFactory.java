package com.matevskial.springreference.service.staticdata;

public class StaticDataFactory {
    public StaticData createStaticData(String listImplementation) {
        switch (listImplementation) {
            case "linkedlist":
                return new LinkedListStaticData();
            case "arraylist":
                return new ArrayListStaticData();
            default:
                return new ArrayListStaticData();
        }
    }
}
