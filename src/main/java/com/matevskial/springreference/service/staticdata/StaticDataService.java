package com.matevskial.springreference.service.staticdata;

/**
 * A set of utility methods to work with StaticData
 */
public interface StaticDataService {
    /**
     * Uses StaticData to retreive lsit of emails
     * @return string representing random email
     */
    String getRandomEmail();
}
