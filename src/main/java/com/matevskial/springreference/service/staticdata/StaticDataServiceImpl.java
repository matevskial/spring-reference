package com.matevskial.springreference.service.staticdata;

import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

@Service
public class StaticDataServiceImpl implements StaticDataService {

    private StaticData staticData;

    public StaticDataServiceImpl(StaticData staticData) {
        this.staticData = staticData;
    }

    @Override
    public String getRandomEmail() {
        List<String> emails = staticData.getEmails();
        int idx = ThreadLocalRandom.current().nextInt(emails.size());

        return emails.get(idx);
    }
}
