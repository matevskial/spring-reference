package com.matevskial.springreference.didemo;

import com.matevskial.springreference.service.mlog.LogService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;

class ConstructorInjectedBeanTest {

    private ConstructorInjectedBean constructorInjectedBean;

    @Mock
    LogService logService;

    @BeforeEach
    void setUp() {
        logService = Mockito.mock(LogService.class);
        constructorInjectedBean = new ConstructorInjectedBean(logService);
    }

    @Test
    void doStuffShouldWork() {
        // Set up behavior of mocked objects
        doNothing().when(logService).log(anyString());

        // When
        String result = constructorInjectedBean.doStuff();

        // Then
        assertEquals("done", result);
    }
}