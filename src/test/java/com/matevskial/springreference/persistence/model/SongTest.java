package com.matevskial.springreference.persistence.model;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SongTest {

    private Song song;

    @BeforeEach
    void setUp() {
        song = new Song();
    }

    @Test
    void getArtistsShouldBeNonNull() {
        assertNotEquals(null, song.getArtists());
    }
}