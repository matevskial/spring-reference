package com.matevskial.springreference.persistence.repository;

import com.matevskial.springreference.persistence.model.Song;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class SongRepositoryIT {

    @Autowired
    SongRepository songRepository;

    @Test
    void findByNameShouldFindOneSong() {
        Optional<Song> optionalSong = songRepository.findByName("Cloudburst Flight");
        assertTrue(optionalSong.isPresent());
        assertEquals("Cloudburst Flight", optionalSong.get().getName());
    }

    @Test
    void  findByAlbumNameShouldFindONonEmptyCollectionOfSongs() {
        Optional<List<Song>> optionalSongs =  songRepository.findByAlbumName("Force Majeure");
        assertTrue(optionalSongs.isPresent());
        assertTrue(optionalSongs.get().size() > 0);
    }
}