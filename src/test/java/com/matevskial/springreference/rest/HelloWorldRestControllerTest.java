package com.matevskial.springreference.rest;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class HelloWorldRestControllerTest {

    HelloWorldRestController helloWorldRestController;

    @BeforeEach
    void setUp() {
        helloWorldRestController = new HelloWorldRestController();
    }

    @Test
    void getHelloWorldShouldReturnStatusCodeOk() throws Exception {
        MockMvc mockMvc = MockMvcBuilders.standaloneSetup(helloWorldRestController).build();
        mockMvc.perform(MockMvcRequestBuilders.get("/"))
                .andExpect(status().isOk());
    }
}