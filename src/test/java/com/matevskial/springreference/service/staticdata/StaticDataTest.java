package com.matevskial.springreference.service.staticdata;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StaticDataTest {

    private StaticData staticData;

    public StaticDataTest() {
        staticData = new StaticDataFactory().createStaticData("arraylist");
    }

    @Test
    void getEmailsShouldGetNonEmptyCollectionOfEmails() {
        assertTrue(staticData.getEmails().size() > 0);
    }
}